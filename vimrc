" Vim ASCII and authorship {
"  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
"  .                                                                         .
"  .                                                                         .
"  .                                 ,ji,                                    .
"  .          ,@HHHHHHHHHHHHHHHHHHH "!:!!Y*,  ,HHHHHHHHHHHHHHHHHHHt          .
"  .          'QYY?00000000000VYYYT ;!!!!!!<= <2YYV00000000000002Yr          .
"  .              #000000000001I ,..!!!!!!!!!=  vd80000000000PY=^            .
"  .              #00U000U000U1I ;!!!!!!!:!' vdH800000000U0YI^               .
"  .              #000000000001I ;!!!!!!'`vd9t00%0000000Y*^`                 .
"  .              #000000000001I ;!!!'`vd9S%%00000000V1^`.                   .
"  .              #0000U00U00U1I :'`,dPH%0000000002Yr^,.!=T),                .
"  .          .;` #000000000001I ,aPHJ0000000000Y/^ .=!!!!+!TT*,             .
"  .       .,VY:` #000U000000014MHJ%00000^"''''^ .=!!!!!!!!!:!!<Ts,          .
"  .       '=!:!` #000000000U0mG00000000"' hnv  +!!!!!!!:!!!!!!=:``          .
"  .          `=' #0000000U000000000000V" d%%" ;!!!!!!:!!!!!:=``             .
"  .            ` #000U0000000000000VY`'       ''``'"""'``''`                .
"  .              #000000000000U00Yr^ ' U002 . "U00N>=vJ0UNn>=JU00\          .
"  .              #0000000U000U?*^ .<` JU02 .' J00^.-- J00~   .U00'          .
"  .              #0000U0000%*^`-=!!` JU02 .  J00F-`  J00(    d%02           .
"  .              #0000002Y'` -^!!!" J002    J002    JU02    J002            .
"  .              YPYYYY='     `'===......<=                                 .
"  .                              `==!!+!=``                                 .
"  .                                 `-'`         Config File                .
"  .                                                    by jhenahan          .
"  .                                                                         .
"  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
"
" }

" Basics {
    " Compat and clipboards {
        set nocompatible " must be first line

        if $SHELL =~ 'fish'
            set shell=zsh
        endif
        if has ("unix") && "Darwin" != system("echo -n \"$(uname)\"")
            set clipboard=unnamedplus
        else
            set clipboard=unnamed
        endif
    " }
    " Sanity {
        filetype indent plugin on " filetypes are nice
        syntax on                 " highlighting is nicer
        scriptencoding utf-8
    " }
" }

" VAM {
    function SetupVAM()
        let c = get(g:, 'vim_addon_manager', {})
        let g:vim_addon_manager = c
        let c.plugin_root_dir = expand('$HOME', 1) . '/.vim/vim-addons'
        let &rtp.=(empty(&rtp)?'':',').c.plugin_root_dir.'/vim-addon-manager'
        if !isdirectory(c.plugin_root_dir.'/vim-addon-manager/autoload')
            execute '!git clone --depth=1 git://github.com/MarcWeber/vim-addon-manager '
                \ shellescape(c.plugin_root_dir.'/vim-addon-manager', 1)
        endif
        call vam#ActivateAddons([], {'auto_install' : 1})
    endfunction

    call SetupVAM()
" }

" Globals {
" }

" Mappings {
    " Switch off search highlighting
    noremap <silent> <Leader>/ :nohlsearch<CR>
    " Move around more easily {
        nnoremap j gj
        nnoremap k gk
    " }
    " Neocomplete mappings {
        inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
        inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
    " }
    " Reflow text {
        " in normal mode...
        nmap <Leader>r gqip
        " ... and selected text
        nmap <Leader>r gq
    " }
    " For consistency
    nnoremap Y y$

    map <Space> :
    map <CR> <Leader>
    let mapleader=''
" }

" Settings {
    " Codekit Compatibility {
    " The mechanism CodeKit uses to detect file changes doesn't play nice with
    " backups and swap files.
        set nobackup
        set nowritebackup
        set noswapfile
    " }
    " Editor stuff {
        set nohidden                  " Abandoned buffers are unloaded.
        set nowrap                    " Because who wraps lines in a text editor?
        set backspace=2               " Backspace over anything
        set pastetoggle=<F12>         " sane paste behavior
        set showmatch                 " show matching delimiters
        set whichwrap=b,s,h,l,>,<,],[ " Everything wraps
        set scrolljump=8              " lines to scroll when cursor leaves screen
        set scrolloff=4               " minimum lines above or below cursor
        set foldenable                " code folding
        if has('persistent_undo')
            set undofile
            set undolevels=1000
            set undoreload=10000
        endif
        " Command line {
            set wildmenu                   " show completion list
            set wildmode=list:longest,full " tab completion
        " }
    " }
    " Formatting {
        set shiftwidth=4   " 4 space indents
        set tabstop=4      " 4 column tabstops
        set softtabstop=-1 " Takes value from tabstop
        set expandtab      " tabs are spaces
    " }
    " Search {
        set ignorecase " Ignore case in searches
        set smartcase  " Unless the search includes capital letters
        set incsearch  " Incremental search is helpful
        set hlsearch   " Highlight matches
    " }
    " UI {
        set number         " Line numbers are good
        set relativenumber " Relative line numbers are awesome
        set showcmd        " Makes visual selection pretty convenient
        set cursorline     " highlight current line
        set cursorcolumn   " highlight current column
        set laststatus=2   " always show status line
        set title          " show title of active file
        set showmode       " show current mode
        set tabpagemax=15  " a reasonable maximum number of tabs to show
        " Set colorcolumn to 80 and mark range beyond 120
        let &colorcolumn="80,".join(range(120,999),",")
        " Colorscheme {
            ActivateAddons Solarized
            colorscheme solarized
        " }
    " }
    " Ignore filenames {
        setlocal suffixes+=
        \.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,
        \.idx,.ilg,.inx,.out,.toc,.pyc,.pyo,
        \.jpg,.bmp,.gif,.png,.tif,.tiff,
        \.wmv,.avi,.mpg,.mpeg,.asf,.flv,.mov,
        \.wav,.aif,.aiff,.mp3,.flac,.mp4,
        \.pdf,.hi,.gz,.fls,.fdb_latexmk
    " }
" }

" Addons {
    " Neocomplete settings {
        let g:acp_enableAtStartup = 0                           " Disable AutoComplPop.
        let g:neocomplete#enable_at_startup = 1                 " Use neocomplete.
        let g:neocomplete#enable_smart_case = 1                 " Use smartcase.
        let g:neocomplete#sources#syntax#min_keyword_length = 0 " Set minimum syntax keyword length.
        let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'
        let g:neocomplete#sources#dictionary#dictionaries = { 'default': '' }
        if !exists('g:neocomplete#keyword_patterns')
            let g:neocomplete#keyword_patterns = {}
        endif
        let g:neocomplete#keyword_patterns['default'] = '\h\w*'
        if !exists('g:neocomplete#sources#omni#input_patterns')
            let g:neocomplete#sources#omni#input_patterns = {}
        endif
        if !exists('g:neocomplete#force_omni_input_patterns')
            let g:neocomplete#force_omni_input_patterns = {}
        endif
        ActivateAddons neocomplete
        ActivateAddons neosnippet
    " }
    " Airline settings
        let g:airline_powerline_fonts = 1
        let g:airline#extensions#tabline#enabled = 1
        ActivateAddons vim-airline
    " }
    " Git settings {
        ActivateAddons fugitive
        ActivateAddons vim-signify
    " }
    " Text interaction {
        ActivateAddons textobj-entire
        ActivateAddons Tabular
    " }
    " Unite settings {
        ActivateAddons vimproc
        ActivateAddons unite
        nnoremap <C-p> :Unite -start-insert file_rec/async<CR>
        ActivateAddons ag
        nnoremap <Leader>. :Unite grep:.<CR>
        nnoremap <Leader>b :Unite -quick-match buffer<CR>
    " }
    " Surround {
        ActivateAddons surround
    " }
    " auto-pairs {
        ActivateAddons Auto_Pairs
    " }
    " sneak {
        ActivateAddons vim-sneak
    " }
    " Rainbow {
        let g:rainbow_active = 1
        ActivateAddons Rainbow_Parentheses_Improved
    " }
    " Syntastic {
        ActivateAddons Syntastic
    " }
    " Haskell {
        let g:necoghc_enable_detailed_browse = 1
        let g:haskell_conceal = 1
        let g:haskell_conceal_wide = 1
        let g:haskell_conceal_bad = 1
        let g:haskell_conceal_enumerations = 0
        let g:ghcmod_ghc_options = ['-package-db', '.cabal-sandbox/x86_64-osx-ghc-7.6.3-packages.conf.d']
        let g:syntastic_haskell_ghc_mod_args = '-g -package-db=.cabal-sandbox/x86_64-osx-ghc-7.6.3-packages.conf.d'
        let g:syntastic_haskell_checkers = ['ghc-mod', 'hlint']
        ActivateAddons ghcmod
        ActivateAddons github:dag/vim2hs
        ActivateAddons github:eagletmt/neco-ghc
        au BufWritePost *.hs GhcModCheckAndLintAsync
    " }
    " Rust {
        ActivateAddons github:wting/rust.vim
    " }
    " Ruby {
        ActivateAddons vim-ruby
        let g:neocomplete#force_omni_input_patterns.ruby = '[^. *\t]\.\w*\|\h\w*::'
    " }
    " LaTeX {
        let g:tex_flavor = "latex"
        ActivateAddons LaTeX_Box
    " }
    " Repeat {
        ActivateAddons repeat
    " }
    " Abolish {
        ActivateAddons abolish
    " }
    " Speed dating {
        ActivateAddons speeddating
    " }
    " Characterize {
        ActivateAddons characterize
    " }
    " Endwise {
        ActivateAddons endwise
    " }
    " Vinegar {
        ActivateAddons github:tpope/vim-vinegar
    " }
" }

" Functions {
    function! s:my_cr_function()
        return pumvisible() ? neocomplete#close_popup() : "\<CR>"
    endfunction

    function! InitializeDirectories()
        let separator = "."
        let parent    = $HOME
        let prefix    = '.vim'
        let dir_list  = { 'views'  : 'viewdir' }

        if has('persistent_undo')
            let dir_list['undo'] = 'undodir'
        endif

        for [dirname, settingname] in items(dir_list)
            let directory = parent . '/' . prefix . dirname . "/"
            if exists("*mkdir")
                if !isdirectory(directory)
                    call mkdir(directory)
                endif
            endif
            if !isdirectory(directory)
                echo "Warning: Unable to create backup directory: " . directory
                echo "Try: mkdir -p " . directory
            else
                let directory = substitute(directory, " ", "\\\\ ", "g")
                exec "set " . settingname . "=" . directory
            endif
        endfor
    endfunction
    call InitializeDirectories()

    function! NeatFoldText()
        let line = ' ' . substitute(getline(v:foldstart), '^\s*"\?\s*\|\s*"\?\s*{{' . '{\d*\s*', '', 'g') . ' '
        let lines_count = v:foldend - v:foldstart + 1
        let lines_count_text = '| ' . printf("%10s", lines_count . ' lines') . ' |'
        let foldchar = matchstr(&fillchars, 'fold:\zs.')
        let foldtextstart = strpart('+' . repeat(foldchar, v:foldlevel*2) . line, 0, (winwidth(0)*2)/3)
        let foldtextend = lines_count_text . repeat(foldchar, 8)
        let foldtextlength = strlen(substitute(foldtextstart . foldtextend, '.', 'x', 'g')) + &foldcolumn
        return foldtextstart . repeat(foldchar, winwidth(0)-foldtextlength) . foldtextend
    endfunction
    set foldtext=NeatFoldText()
" }
