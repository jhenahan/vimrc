" Formatting {
    setlocal shiftwidth=2   " 4 space indents
    setlocal tabstop=2      " 4 column tabstops
    setlocal softtabstop=-1 " Takes value from tabstop
    setlocal expandtab      " tabs are spaces
" }

setlocal omnifunc=rubycomplete#Complete
compiler ruby

let maplocalleader=','

nmap <LocalLeader>m :make %<CR>
